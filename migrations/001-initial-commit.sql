-- Up

CREATE TABLE DOCUMENTS(
  id INTEGER PRIMARY KEY,
  uploadedBy TEXT,
  filesize INTEGER DEFAULT null,
  vendorName TEXT DEFAULT null,
  invoiceDate DATETIME DEFAULT null,
  amountDue REAL DEFAULT null,
  currency TEXT DEFAULT null,
  taxAmount REAL DEFAULT null,
  processingStatus Text DEFAULT null,

  uploadedTimestamp DATETIME DEFAULT CURRENT_TIMESTAMP
);


INSERT INTO DOCUMENTS(id, uploadedBy, filesize, vendorName, invoiceDate, amountDue, currency, taxAmount, processingStatus) 
Values (1, 'psdesai.93@gmail.com', 240, 'hubdoc', '20th March, 2019', 100, 'CAD', 10, '');
-- Down
DROP TABLE DOCUMENTS;