Notes:
I wasn't sure which one exactly is the vendor name, so for the vendorName i went with the immediate value after "Invoice #" and "Date". 

I wasn't sure of the processinStatus as well, so i have left it blank. 

Currently, i delete the uploaded document from disk, once the data has been extracted and saved, but ideally i would store it in a document store (think S3) and save the key in the table. 

I have gone with sqlite as the data store since it's super easy to set up and get it going. For this particular use case, a No SQL db would make sense as well.

With invoiceDate, i'm storing the date in the format that is in the pdf document, but a case can be made to convert it into a unix timestamp to store it and then format it in the get api call. 

To run: i) clone the repo, ii) run `npm i` and iii) run `npm run dev`. Once the dev server is running, you can use the curl commands mentioned in the readme to upload and get the documents. 

To run the integration tests, run `npm test`. 
