const request = require('supertest');
const assert = require('chai').assert;
const expect = require('chai').expect;

describe('documents server', function() {
  var server;
  before(function() {
    server = require('../index');
  });

  after(function() {
    server.close();
  });

  describe('sanity check', function() {
    it('working?', function(done) {
      request(server)
        .get('/')
        .expect(
          200,
          {
            data: 'Hello Hubdoc!'
          },
          done
        );
    });

    it('returns an empty document for non-existent document', function(done) {
      request(server)
        .get('/document/xyz')
        .expect(200, {}, done);
    });

    it('returns an existing document', function(done) {
      request(server)
        .get('/document/1')
        .expect(200)
        .end(function(err, res) {
          if (err) {
            done(err);
          }

          let response = JSON.parse(res.text);
          expect(response).to.have.keys([
            'amountDue',
            'currency',
            'filesize',
            'id',
            'invoiceDate',
            'processingStatus',
            'taxAmount',
            'uploadedBy',
            'uploadedTimestamp',
            'vendorName'
          ]);
          done();
        });
    });

    it('should require a request body', function(done) {
      request(server)
        .post('/upload')
        .expect(400, { error: 'invalid_params' }, done);
    });

    it('should require an email', function(done) {
      request(server)
        .post('/upload')
        .field('emai', 'user@example.com')
        .expect(400)
        .end(function(err, res) {
          assert(res.status == 400);
          done();
        });
    });

    it('should require a valid email', function(done) {
      request(server)
        .post('/upload')
        .field('email', 'xyz')
        .expect(400)
        .end(function(err, res) {
          assert(res.status == 400);
          done();
        });
    });

    it('should insert a pdf document', function(done) {
      request(server)
        .post('/upload')
        .field('email', 'user@example.com')
        .attach('file', './invoices/hubdocInvoice1.pdf')
        .expect(200)
        .end(function(err, res) {
          if (err) done(err);
          let response = JSON.parse(res.text);
          expect(response).to.have.property('id');
          done();
        });
    });
  });
});
