const Joi = require('@hapi/joi');

const uploadSchema = {
  email: Joi.string()
    .regex(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)
    .required()
};

module.exports = {
  uploadSchema
};
