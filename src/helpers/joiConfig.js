const Joi = require('@hapi/joi');

const VALIDATION_OPTS = {
  abortEarly: false
};

const joiValidate = (params, schema, opts = VALIDATION_OPTS) => {
  const { error, value } = Joi.validate(params, schema, opts);

  if (error) {
    throw new Error(error);
  }

  return;
};

module.exports = {
  joiValidate
};
