const NP = require('number-precision');

const parseFields = text => {
  const textArr = text.split('\n').filter(ele => ele.trim().length > 0);

  const currency = findCurrency(textArr);
  const invoiceDate = findDate(textArr);
  let amountDue = findTotal(textArr);

  const vendorName = findVendor(textArr);
  let [taxType, taxPercent, taxAmount] = findTax(textArr);

  amountDue = parseFloatToPrecison(amountDue, 2);
  taxAmount = parseFloatToPrecison(taxAmount, 2);

  return {
    currency,
    invoiceDate,
    amountDue,
    vendorName,
    taxType,
    taxPercent,
    taxAmount
  };
};

const findCurrency = textArr => {
  let END_LINE = 'Thank you for your business';
  for (let i = 0; i < textArr.length; i++) {
    let currVal = textArr[i].trim();
    if (currVal.indexOf(END_LINE) > -1) {
      return textArr[i - 1];
    }
  }

  return '';
};

const findVendor = textArr => {
  const INVOICE_REGEX = /invoice\W{1,}[0-9]+/gim;
  for (let i = 0; i < textArr.length; i++) {
    let currVal = textArr[i].trim();
    if (INVOICE_REGEX.test(currVal) && i + 2 < textArr.length) {
      return textArr[i + 2];
    }
  }
  return '';
};

const findDate = textArr => {
  const INVOICE_REGEX = /invoice\W{1,}[0-9]+/gim;
  for (let i = 0; i < textArr.length; i++) {
    let currVal = textArr[i].trim();
    if (INVOICE_REGEX.test(currVal) && i + 1 < textArr.length) {
      let date = textArr[i + 1];
      return date;
    }
  }

  return '';
};

const findTotal = textArr => {
  const TOTAL_REGEX = /^total.([0-9]{0,}.[0-9]{0,})/gim;
  for (let i = 0; i < textArr.length; i++) {
    let currVal = textArr[i].trim();
    let val = TOTAL_REGEX.exec(currVal);

    if (val && val[1]) {
      return val[1];
    }
  }

  return '';
};

const findTax = textArr => {
  const taxes = ['tax', 'gst'].join('|');
  const regexStr = `(${taxes})\\s([0-9]+%).([0-9.]+)`;

  const TAX_REGEX = new RegExp(regexStr, 'gmi');
  for (let i = 0; i < textArr.length; i++) {
    let currVal = textArr[i].trim();
    let val = TAX_REGEX.exec(currVal);

    if (val && val.length >= 3) {
      return [val[1], val[2], val[3]];
    }
  }

  return ['', '', ''];
};

const parseFloatToPrecison = (floatNum = '', precision) => {
  if (!floatNum) {
    return '';
  }
  floatNum = parseFloat(floatNum);
  return floatNum.toFixed(precision);
};

module.exports = { parseFields };
