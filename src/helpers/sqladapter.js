const sqlite = require('sqlite');

const sqliteInit = () => {
  const dbPromise = Promise.resolve()
    .then(() => sqlite.open('./database.sqlite', { Promise }))
    .then(db => db.migrate());
};

module.exports = { sqliteInit };
