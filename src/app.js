const DocumentModel = require('./models/document');
const { sqliteInit } = require('./helpers/sqladapter');
const { uploadSchema } = require('./helpers/schema');
const { joiValidate } = require('./helpers/joiConfig');

const express = require('express');
const multer = require('multer');
const fs = require('fs');

const port = 3000;
const app = express();
const upload = multer({ dest: 'uploads/' });

sqliteInit();

//sanity check
app.get('/', async (req, res) => {
  res.status(200).send({ data: 'Hello Hubdoc!' });
});

app.get('/document/:id', async (req, res) => {
  try {
    let { id } = req.params;
    const Document = new DocumentModel();
    const doc = await Document.get(id);

    res.status(200).json({ ...doc });
  } catch (error) {
    res.status(404).json({ error: 'Something went wrong! :(' });
  }
});

app.post('/upload', upload.single('file'), async (req, res) => {
  try {
    if (!req.body) {
      throw new Error('invalid_params');
    }

    joiValidate(req.body, uploadSchema);

    let { email: uploadedBy = '' } = req.body;
    let { path, size: filesize } = req.file;

    const Document = new DocumentModel();
    let values = await Document.readFile(path);

    const id = await Document.insert({
      ...values,
      uploadedBy,
      filesize
    });

    //delete the uploaded document
    //ideally this would happen once the doc has been saved to a storage bucket
    await fs.unlinkSync(path);

    res.status(200).json({ id });
  } catch (error) {
    let { message = 'Something went wrong' } = error;
    res.status(400).json({ error: message });
  }
});

module.exports = app;
