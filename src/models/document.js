const { parseFields } = require('../helpers/parsePdf');

const fs = require('fs');
const pdf = require('pdf-parse');
const sqlite = require('sqlite');

class Document {
  constructor() {}

  async readFile(filePath = '') {
    const dataBuffer = fs.readFileSync(filePath);
    let parsed = await this.parse(dataBuffer);

    return parsed;
  }

  async get(id) {
    let query = Document.getStatement();
    let data = await sqlite.get(query, { $id: id });

    return data;
  }

  async insert(values) {
    let defaults = Document.defaults();
    values = { ...defaults, ...values };

    let query = Document.insertStatement();

    let insert = await sqlite.run(query, {
      $uploadedBy: values.uploadedBy,
      $filesize: values.filesize,
      $vendorName: values.vendorName,
      $invoiceDate: values.invoiceDate,
      $amountDue: values.amountDue,
      $currency: values.currency,
      $taxAmount: values.taxAmount,
      $processingStatus: values.processingStatus
    });

    const { lastID } = insert.stmt;
    return lastID;
  }

  //using $variable means we don't have to worry about sql escaping
  static getStatement() {
    return `Select * from Documents where id = $id`;
  }

  static insertStatement() {
    return `INSERT INTO DOCUMENTS 
      (uploadedBy, filesize, vendorName, invoiceDate, amountDue, currency, taxAmount, processingStatus) 
      Values ($uploadedBy, $filesize, $vendorName, $invoiceDate, $amountDue, $currency, $taxAmount, $processingStatus)`;
  }

  parse(dataBuffer) {
    return pdf(dataBuffer).then(data => {
      let { text } = data;
      return parseFields(text);
    });
  }

  static defaults() {
    return {
      filesize: '',
      vendorName: '',
      invoiceDate: '',
      amountDue: '',
      currency: '',
      taxAmount: '',
      uploadedBy: '',
      processingStatus: ''
    };
  }
}

module.exports = Document;
